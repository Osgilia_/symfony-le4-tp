<?php


namespace App\Command;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class HelloWorldCommand extends Command {
    protected static $defaultName = 'hello:world';
    protected function configure()
    {
        $this
            ->setDescription("Prints Hello World!")
            ->addArgument(
                'name',
                InputArgument::OPTIONAL,
                '',
                'World'
            )
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output) {
        $name = $input->getArgument('name');
        $output->writeln("Hello $name");
        $log = new Logger('Hello_Logger');
        $log->pushHandler(new StreamHandler('logs/hello.log', Logger::WARNING));
        $log->warning("Hello $name");

        return 0;
    }
}
<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\ArticleDoctrine;
use App\Entity\Comment;
use App\DataFixtures\AppFixtures;
use Faker;

class CommentFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create();

        for( $i = 0; $i < 10; $i++) {
            $ref = 'article-'.$i;

            for($j = 0; $j < rand(0, 9); $j++) {
                $comment = new Comment();
                $comment->setArticle($this->getReference($ref));
                $comment->setName($faker->name);
                $comment->setEmail($faker->email);
                $comment->setCreatedAt($faker->dateTime);
                $comment->setComment($faker->text);
                $comment->getArticle()->addComment($comment);
                $manager->persist($comment);
                $manager->flush();
            }
        }
    }
}

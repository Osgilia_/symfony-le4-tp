<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use App\Entity\ArticleDoctrine;
use PhpParser\Node\Expr\Array_;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        $faker = Faker\Factory::create();
        for( $i = 0; $i < 10; $i++) {
            $article = new ArticleDoctrine();
            $article->setTitle($faker->title);
            $article->setAuthor($faker->name);
            $article->setBody($faker->text($maxNbChars = 200));
            $article->setImage($faker->imageUrl($width = 640, $height = 480));
            $article->setSubtitle($faker->title);
            $article->setCreatedArt($faker->dateTime);

            $manager->persist($article);
            $ref_article = 'article-'.$i;
            $this->addReference($ref_article, $article);
            $manager->flush();

        }

    }
}

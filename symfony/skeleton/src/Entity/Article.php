<?php

namespace App\Entity;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class Article
{
    /**
     * @var title
     * @Assert\Type("string")
     */
    private $title;
    /**
     * @var subtitle
     * @Assert\Type("string")
     */
    private $subtitle;
    /**
     * @var createAt
     * @Assert\Type("DateTime")
     *
     */
    private $createdAt;
    /**
     * @var author
     * @Assert\Type("string")
     */
    private $author;

    /**
     * @var body
     * @Assert\Type("string")
     */
    private $body;

    /**
     * @var image
     * @Assert\Type("string")
     */
    private $image;

    public function setTitle( string $newTitle) {
        $this->title = $newTitle;
    }

    public function getTitle(): string {
        return $this->title;
    }

    public function setBody( string $newBody) {
        $this->body = $newBody;
    }

    public function getBody(): string {
        return $this->body;
    }

    public function setSubtitle( string $newSubTitle) {
        $this->subtitle = $newSubTitle;
    }

    public function getSubtitle(): string {
        return $this->title;
    }

    public function setAuthor( string $newAuthor) {
        $this->author = $newAuthor;
    }

    public function getAuthor(): string {
        return $this->author;
    }

    public function setCreatedAt(DateTime $newDate) {
        $this->createdAt = $newDate;
    }

    public function getCreatedAt(): string {
        return $this->createdAt->format("Y-m-d H:i:s");
    }

    public function setImage( string $filePath) {
        $this->image = $filePath;
    }

    public function getImage(): string {
        return $this->image;
    }

    public function __construct()
    {
        $this->title = "Titre";
        $this->subtitle = "Sous-Titre";
        $this->author = "Henri Defrance";
        $this->body = "Lorem ipsum dolor sit amet";
        $this->createdAt = new DateTime();
        $this->image = "";

    }
}

<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class HelloController extends AbstractController
{
    /**
     * @Route("/hello/{name}", name="hello_name")
     */
    public function helloName($name)
    {
        //return new Response('Hello ' . $request->get('name'));
        return new Response('Hello ' . $name);

    }

    /**
     * @Route("/hello", name="hello")
     */
    public function index()
    {
        $user = [
            'name' => 'Henri',
            'place' => 'Lens'
        ];
        return $this->render('hello/index.html.twig', [
            'user' => $user
        ]);
    }
}
